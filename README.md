# Proyecto #2 de Introducción al Reconocimiento de Patrones

# Dependencias

El proyecto utiliza algunas bibliotecas que no se encuentran
instaladas por defecto. Para esto puede utiliza `pip3`:

```bash
python3 -m pip install gym --user
python3 -m pip install atari_py --user
```

# Ejecución

Para ejecutar el juego, se debe abrir una consola en la
carpeta donde se ubica este archivo. Es recomendado acceder
a la carpeta de la red que se quiere utilizar:

## Dueling Network

Para acceder a la carpeta:

```bash
cd DQN-Dueling
```

Para ver todos las posibles banderas que se pueden utilizar,
se ejecuta:

```bash
python3 atari.py -h
```

Para empezar el entrenamiento de la red con visualización
del progreso, se utiliza:

```bash
python3 atari.py -t -v
```


## Base Network

Para acceder a la carpeta:

```bash
cd deep-q-base
```

Para ver todos las posibles banderas que se pueden utilizar,
se ejecuta:

```bash
python3 atari.py -h
```

Para empezar el entrenamiento de la red con visualización
del progreso, se utiliza:

```bash
python3 atari.py -t -v
```


## Conv Network
Esta red está basada en la red base, pero eliminando la tercera capa convolucional


Para acceder a la carpeta:

```bash
cd deep-q-conv
```

Para ver todos las posibles banderas que se pueden utilizar,
se ejecuta:

```bash
python3 atari.py -h
```

Para empezar el entrenamiento de la red con visualización
del progreso, se utiliza:

```bash
python3 atari.py -t -v
```




## Learning rate Network
Esta red está basada en la red base, pero modificando la tasa de aprendizaje

Para acceder a la carpeta:

```bash
cd deep-q-learning_rate
```

Para ver todos las posibles banderas que se pueden utilizar,
se ejecuta:

```bash
python3 atari.py -h
```

Para empezar el entrenamiento de la red con visualización
del progreso, se utiliza:

```bash
python3 atari.py -t -v
```


